Варіант структуру проекту з розбиттям файлі по типу.

```
store.js
App.js
index.js
actions/
    types.js (константи екшенов зберігаємо тут)
    playerActions.js
    {YOUR_NAME}Actions.js
components/ (маленькі і середні компоненти, кнопки, форми, лісти)
    AudioPlayer.js
    {COMPONENT_NAME}.js
containers/ (компоненти-контейнери(сторінки))
    AdminBox.js 
    {CONTAINER_NAME}.js
reducers/
    index.js  (rootReducer)
    playerReducer.js
    {YOUR_NAME}Reducer.js

```