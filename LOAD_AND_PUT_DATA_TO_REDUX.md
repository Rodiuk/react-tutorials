1. В файлі `types.js` створити константу 
```
export const ALBUMS_LOADED равно 'ALBUMS_LOADED';
```

2. Створити файл з `albumsActions.js` з кодом
```
export const loadAlbums = () => dispatch => {
    fetch('/api/albums')
        .then(res => res.json())
        .then(data => dispatch({type:ALBUMS_LOADED, payload: data}))
}
```
3. Додати редюсер, котрий буде ловити цей івент. і додати його в рут редюсер


const initialState = [];


function albumsReducer (state = initialState, action){
    switch(action.type) {
        case 'ALBUMS_LOADED':
            return [...action.payload];
        default:
            return state;
    }
}

export default albumsReducer
```

4. в Компоненті `App.js` підвантажити дані 

import {connect} from 'react-redux'

componentDidMount(){
        if (this.props.albums.length === 0) {
            this.props.loadData();
        }
    }


const mapStateToProps = state => ({
    albums: state.albums
});

const mapDispatchToProps = dispatch => ({
    loadData: () => dispatch(loadAlbums())
});


export default connect(mapStateToProps, mapDispatchToProps)(App)

```
5. Add albumsReducer to rootReducer via {combineReducer}

```
cosnt rootReducer = combineReducers({
 albums: albumsReducer
})
```
