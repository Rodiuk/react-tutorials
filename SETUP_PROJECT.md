1. За допомогою `create-react-app` створити проект.
```
create-react-app FOLDER_NAME
```

1. Встановити додаткові пакети(залежності).

```
cd FOLDER_NAME
npm install --save react-router-dom redux react-redux redux-thunk redux-devtools-extension
```

1. Видаляємо все що в середині директорії `src` і працюємо далі там (робимо все в ній).

```
rm -f src/*
```

1. Створюєму `index.js` з кодом що нижче.

```
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {BrowserRouter, Route} from 'react-router-dom'
import App from './App';
import store from './store'

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Route path={'/'} component={App}/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

```

1. Create `store.js`.

```
import {createStore, applyMiddleware} from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk'

function rootReducer (state={}, action){
    return state;
}

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
export default store;
```

1. Створюємо `App.js`.

```
import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom'

class App extends Component {
  render() {
    return (
        <div>
            <Switch>
                <Route exact path={'/'} component={()=><h2>YOUR_HOME_COMPONENT</h2>}/>
                <Route exact path={'/other'} component={()=><h2>YOUR_OTHER_COMPONENT</h2>}/>
            </Switch>
        </div>
    );
  }
}

export default App;

```

1. На виході маємо отримати таку структуру.

```
src/
  App.js
  store.js
  index.js
 
```

1. Запустити проект виконавши команду, побачити що текст з'явився на екрані.

```
npm run start
```